# text-encoding单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/mixpanel/mixpanel-android/tree/master/src/androidTest/java/com/mixpanel/android/mpmetrics)
进行单元测试

单元测试用例覆盖情况

|                                                     接口名                                                     |                    是否通过	                     |备注|
|:-----------------------------------------------------------------------------------------------------------:|:--------------------------------------------:|:---:|
|                                      init(token:string, config:object)                                      |                     pass                     |       |
|                          track(event:string, properties:object, callback:function)                          |                     pass                     |        |
|                       trackBatch(eventList:array, options:object, callback:function)                        |                     pass                     |        |
|               import(event:string, time:data or number, properties:object, callback:function)               |pass   |        |
|                       importBatch(eventList:array, options:object, callback:function)                       |                     pass                     |        |
|                                   alias(distinctId:string, alias:string)                                    |                     pass                     |        |
|                                          setConfig(config:object)                                           |                     pass                     |     |
| setOnce(groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function) |                     pass                     |          |
|   set(groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function)   |                     pass                     |  |
|              deleteGroup(groupKey:string, groupId:string, modifiers:object, callback:function)              |                     pass                     |       |
|          remove(groupKey:string, groupId:string, data:object, modifiers:object, callback:function)          |                     pass                     |          |
|          union(groupKey:string, groupId:string, data:object, modifiers:object, callback:function)           |                     pass                     |          |
|       unset(groupKey:string, groupId:string, properties:object, modifiers:object, callback:function)        |                     pass                     |          |
|        setOnce(distinctId:string, properties:object, to:string, modifiers:object, callback:function)        |                     pass                     |          |
|          set(distinctId:string, properties:object, to:string, modifiers:object, callback:function)          |                     pass                     |          |
|       increment(distinctId:string, properties:object, by:string, modifiers:object, callback:function)       |                     pass                     |          |
|       append(distinctId:string, properties:object, value:string, modifiers:object, callback:function)       |                     pass                     |          |
|    trackCharge(distinctId:string, amount:number, properties:object, modifiers:object, callback:function)    |                     pass                     |          |
|                    clearCharges(distinctId:string, modifiers:object, callback:function)                     |                     pass                     |          |
|                     deleteUser(distinctId:string, modifiers:object, callback:function)                      |                     pass                     |       |
|                 remove(distinctId:string, data:object, modifiers:object, callback:function)                 |                     pass                     |       |
|                 union(distinctId:string, data:object, modifiers:object, callback:function)                  |                     pass                     |          |
|              unset(distinctId:string, properties:object, modifiers:object, callback:function)               |                     pass                     |         |
