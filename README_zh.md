# mixpanel

## 简介

 Mixpanel是一个产品分析工具，它使您能够捕获用户与数字产品交互的数据，并允许您使用简单的交互式报告分析此产品数据，您只需要单击几下就可以查询以及可视化数据。

## 下载安装

```shell
ohpm install @ohos/mixpanel
```

OpenHarmony ohpm环境配置等更多内容，请参考[如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明
```
  import mpanel from '@ohos/mixpanel';
```

### 发送事件

```javascript
onButtonTrackClick(){
        this.mpanel.track("Button Click Track");
    },
```

### 发送批事件

```javascript
onButtonBatchClick(){
        this.mpanel.trackBatch(this.batchEvents);
    },
```

### 用户属性仅在之前未设置的情况下将属性值添加 

```javascript
onButtonPeopleSetOnceClick(){
        this.people.setOnce(this.distinctId, {
            'place': 'Ahmedabad',
            'Date': '23 Dec 2021'
        });
    },
```



### 用户配置文件上设置属性

```javascript
onButtonPeopleSetClick(){
        this.people.set(this.distinctId, {
            'gender': 'male',
            'age': '28'
        });
    },
```



### 递增数值属性

```javascript
onButtonPeopleIncrementClick(){
        this.people.increment(this.distinctId, 'page_views', 1000);
    },
```



### 附加到列表属性

```javascript
onButtonPeopleAppendClick(){
        this.people.append(this.distinctId, {
            list1: 'abcd',
            list2: 123
        });
    },
```



### 跟踪收入

```javascript
onButtonPeopleTrackClickCharge(){
        this.people.trackCharge(this.distinctId, 1000);
    },
```



### 取消跟踪收入

```javascript
onButtonPeopleClearClickCharge(){
        this.people.clearCharges(this.distinctId);
    },
```



### 清除特定用户

```javascript
onButtonPeopleDeleteClick(){
        this.people.deleteUser(this.distinctId);
    },
```



### 移除列表中人物属性特定值

```javascript
onButtonPeopleRemoveClick(){
        this.people.remove(this.distinctId, {list1: 'abcd'});
    },
```



### 将指定的值添加到人物属性列表并确保这些值只出现一次。

```javascript
onButtonPeopleUnionClick(){
        this.people.union(this.distinctId, {list1: 'abcd'});
    },
```



### 取消设置人物属性列表中的某个属性值

```javascript
onButtonPeopleUnsetClick(){
        this.people.unset(this.distinctId, ['list1', 'list2']);
    },
```



### 仅在之前未设置的情况下将属性值添加到组中

```javascript
onButtonGroupSetOnceClick(){
        this.group.setOnce(this.groupKey, this.groupId,{
            'name': 'abcd',
            'age':'21',
        });
    },
```



### 更新或添加属性到组

```javascript
onButtonGroupSetClick(){
        this.group.set(this.groupKey, this.groupId,{
            'name': 'santa',
            'age':'26',
        });
    },
```



### 删除一个组 

```javascript
onButtonGroupDeleteClick(){
        this.group.deleteGroup(this.groupKey, this.groupId);
    }
```



### 删除列表属性中的特定值 

```javascript
onButtonGroupRemoveClick(){
        this.group.remove(this.groupKey, this.groupId, {'age':'21'});
    },
```



### 将指定的值添加到列表属性并确保这些值只出现一次 

```javascript
onButtonGroupUnionClick(){
        this.group.union(this.groupKey, this.groupId, {'age':'21'});
    },
```



### 取消设置组中的特定属性

```javascript
onButtonGroupUnsetClick(){
        this.group.unset(this.groupKey, this.groupId, 'age');
    },
```



## 接口说明

### mixpanel

| 接口名      | 参数                                                         | 返回值 | 说明                                                         |
| ----------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| init        | token:string, config:object                                  | void   | mixpanel使用token和config来初始化人物信息                    |
| track       | event:string, properties:object, callback:function           | void   | 向mixpanel发送事件                                           |
| trackBatch  | eventList:array, options:object, callback:function           | void   | 向mixpanel发送一批事件                                       |
| import      | event:string, time:date\|number, properties:object, callback:function | void   | 使用endpoint向mixpanel发送事件<br/>时间参数应为日期或数字，并应表示事件发生的时间。 |
| importBatch | eventList:array, options:object, callback:function           | void   | 用endpoint将事件列表发送到mixpanel                           |
| alias       | distinctId:string, alias:string                              | void   | 为Districted创建别名                                         |
| setConfig   | config:object                                                | void   | 修改mixpanel配置                                             |

### groups

| 接口名      | 参数                                                         | 返回值 | 说明                                                 |
| ----------- | ------------------------------------------------------------ | ------ | ---------------------------------------------------- |
| setOnce     | groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function | void   | 仅当以前未设置属性值时，此函数才能将属性值添加到组中 |
| set         | groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function | void   | 用于设置组配置文件的属性                             |
| deleteGroup | groupKey:string, groupId:string, modifiers:object, callback:function | void   | 用于永久删除组配置文件                               |
| remove      | groupKey:string, groupId:string, data:object, modifiers:object, callback:function | void   | 用于从列表值组配置文件属性中删除值。                 |
| union       | groupKey:string, groupId:string, data:object, modifiers:object, callback:function | void   | 将值合并到列表值组配置文件属性中。                   |
| unset       | groupKey:string, groupId:string, properties:object, modifiers:object, callback:function | void   | 删除组配置文件上的属性。                             |



### people



| 接口名       | 参数                                                         | 返回值 | 说明                                               |
| ------------ | ------------------------------------------------------------ | ------ | -------------------------------------------------- |
| setOnce      | distinctId:string, properties:object, to:string, modifiers:object, callback:function | void   | 仅当当前未设置用户属性时，此方法才允许设置该属性。 |
| set          | distinctId:string, properties:object, to:string, modifiers:object, callback:function | void   | 在engage中设置用户记录的属性。                     |
| increment    | distinctId:string, properties:object, by:string, modifiers:object, callback:function | void   | engage中用户记录的递增/递减属性。                  |
| append       | distinctId:string, properties:object, value:string, modifiers:object, callback:function | void   | 将值附加到列表值的people analytics属性。           |
| trackCharge  | distinctId:string, amount:number, properties:object, modifiers:object, callback:function | void   | 记录您已向当前用户赚取的金额费用。                 |
| clearCharges | distinctId:string, modifiers:object, callback:function       | void   | 清除当前用户的所有交易数据。                       |
| deleteUser   | distinctId:string, modifiers:object, callback:function       | void   | 删除engage中的用户记录。                           |
| remove       | distinctId:string, data:object, modifiers:object, callback:function | void   | 从列表值用户配置文件属性中删除值。                 |
| union        | distinctId:string, data:object, modifiers:object, callback:function | void   | 将值合并到列表值人员分析属性中。                   |
| unset        | distinctId:string, properties:object, modifiers:object, callback:function | void   | 在engage中删除用户记录上的属性。                   |



## 约束与限制
在下述版本验证通过：
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构

```
|---- mixpanel  
|     |---- entry  # 示例代码文件夹
|     |---- library  # mixpanel 库文件夹
|     |---- README.MD  # 安装使用方法  
|     |---- README_zh.md  # 安装使用方法 
```

## 贡献代码

使用过程中发现任何问题都可以提[Issue](https://gitee.com/openharmony-tpc/mixpanel-ohos/issues) 给我们，当然，我们也非常欢迎你给我们提[PR](https://gitee.com/openharmony-tpc/mixpanel-ohos/pulls)。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/mixpanel-ohos/blob/master/LICENSE)，请自由地享受和参与开源。
