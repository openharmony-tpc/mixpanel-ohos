/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Group profile methods. Learn more: https://help.mixpanel.com/hc/en-us/articles/360025333632
 */
import {ProfileHelpers} from './profile_helpers.js'

export class MixPanelGroups extends ProfileHelpers() {
    constructor(mpInstance) {
        super();
        this.mixpanel = mpInstance;
        this.endpoint = '/groups';
    }

    /**
     * groups.setOnce(groupKey, groupId, prop, to, modifiers, callback)
     * ---
     * The same as groups.set, but adds a property value to a group only if it has not been set before.
     */
    setOnce(groupKey, groupId, prop, to, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._set(prop, to, modifiers, callback, { identifiers, setOnce: true });
    }

    /**
     * groups.set(groupKey, groupId, prop, to, modifiers, callback)
     * ---
     * set properties on a group profile
     *
     * usage:
     *     mixpanel.groups.set('company', 'Acme Inc.', '$name', 'Acme Inc.');
     *     mixpanel.groups.set('company', 'Acme Inc.', {
     *         'Industry': 'widgets',
     *         '$name': 'Acme Inc.',
     *     });
     */
    set(groupKey, groupId, prop, to, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._set(prop, to, modifiers, callback, { identifiers });
    }

    /**
     * groups.deleteGroup(groupKey, groupId, modifiers, callback)
     * ---
     * delete a group profile permanently
     * usage:
     * mixpanel.groups.deleteGroup('company', 'Acme Inc.');
     */
    deleteGroup(groupKey, groupId, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._deleteProfile({ identifiers, modifiers, callback });
    }

    /**
     * groups.remove(groupKey, groupId, data, modifiers, callback)
     * ---
     * remove a value from a list-valued group profile property.
     *
     * usage:
     * mixpanel.groups.remove('company', 'Acme Inc.', {'products': 'anvil'});
     * mixpanel.groups.remove('company', 'Acme Inc.', {
     *     'products': 'anvil',
     *     'customer segments': 'coyotes'
     * });
     */
    remove(groupKey, groupId, data, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._remove({ identifiers, data, modifiers, callback });
    }

    /**
     * groups.union(groupKey, groupId, data, modifiers, callback)
     * ---
     * merge value(s) into a list-valued group profile property.
     *
     * usage:
     * mixpanel.groups.union('company', 'Acme Inc.', {'products': 'anvil'});
     * mixpanel.groups.union('company', 'Acme Inc.', {'products': ['anvil'], 'customer segments': ['coyotes']});
     */
    union(groupKey, groupId, data, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._union({ identifiers, data, modifiers, callback })
    }

    /**
     * groups.unset(groupKey, groupId, prop, modifiers, callback)
     * ---
     * delete a property on a group profile
     *
     * usage:
     * mixpanel.groups.unset('company', 'Acme Inc.', 'products');
     * mixpanel.groups.unset('company', 'Acme Inc.', ['products', 'customer segments']);
     */
    unset(groupKey, groupId, prop, modifiers, callback) {
        const identifiers = { $groupKey: groupKey, $groupId: groupId };
        this._unset({ identifiers, prop, modifiers, callback })
    }
}