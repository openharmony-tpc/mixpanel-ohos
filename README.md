# mixpanel

## Overview

Mixpanel is a product analysis tool that enables you to capture data exchanged between users and digital products and allows you to analyze the product data using simple interactive reports. You can query and visualize the data with a few clicks.

## How to Install

```shell
ohpm install @ohos/mixpanel
```

For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use
```
  import mpanel from '@ohos/mixpanel';
```

### Send an event.

```javascript
onButtonTrackClick(){
        this.mpanel.track("Button Click Track");
    },
```

### Send events in batches.

```javascript
onButtonBatchClick(){
        this.mpanel.trackBatch(this.batchEvents);
    },
```

### Add an property value only if the user property has not been set before. 

```javascript
onButtonPeopleSetOnceClick(){
        this.people.setOnce(this.distinctId, {
            'place': 'Ahmedabad',
            'Date': '23 Dec 2021'
        });
    },
```



### Set properties in the user profile.

```javascript
onButtonPeopleSetClick(){
        this.people.set(this.distinctId, {
            'gender': 'male',
            'age': '28'
        });
    },
```



### Increment numerical attributes.

```javascript
onButtonPeopleIncrementClick(){
        this.people.increment(this.distinctId, 'page_views', 1000);
    },
```



### Add attributes to the property list.

```javascript
onButtonPeopleAppendClick(){
        this.people.append(this.distinctId, {
            list1: 'abcd',
            list2: 123
        });
    },
```



### Enable revenue track.

```javascript
onButtonPeopleTrackClickCharge(){
        this.people.trackCharge(this.distinctId, 1000);
    },
```



### Cancel revenue track.

```javascript
onButtonPeopleClearClickCharge(){
        this.people.clearCharges(this.distinctId);
    },
```



### Clear specific users.

```javascript
onButtonPeopleDeleteClick(){
        this.people.deleteUser(this.distinctId);
    },
```



### Delete a specific value from the person property list.

```javascript
onButtonPeopleRemoveClick(){
        this.people.remove(this.distinctId, {list1: 'abcd'});
    },
```



### Add a specific value to the person property list and ensure that the value appears only once.

```javascript
onButtonPeopleUnionClick(){
        this.people.union(this.distinctId, {list1: 'abcd'});
    },
```



### Cancel the setting of an property value in the person property list.

```javascript
onButtonPeopleUnsetClick(){
        this.people.unset(this.distinctId, ['list1', 'list2']);
    },
```



### Add an property value to a group only if it has not set before.

```javascript
onButtonGroupSetOnceClick(){
        this.group.setOnce(this.groupKey, this.groupId,{
            'name': 'abcd',
            'age':'21',
        });
    },
```



### Update or add attributes to a group.

```javascript
onButtonGroupSetClick(){
        this.group.set(this.groupKey, this.groupId,{
            'name': 'santa',
            'age':'26',
        });
    },
```



### Delete a group. 

```javascript
onButtonGroupDeleteClick(){
        this.group.deleteGroup(this.groupKey, this.groupId);
    }
```



### Deletes a specific value from a list property. 

```javascript
onButtonGroupRemoveClick(){
        this.group.remove(this.groupKey, this.groupId, {'age':'21'});
    },
```



### Add a specific value to the list property and ensure that the value appears only once. 

```javascript
onButtonGroupUnionClick(){
        this.group.union(this.groupKey, this.groupId, {'age':'21'});
    },
```



### Unsets a specific property in the group.

```javascript
onButtonGroupUnsetClick(){
        this.group.unset(this.groupKey, this.groupId, 'age');
    },
```



## Available APIs

### mixpanel

| Name     | Parameter                                                        | Return Value | Description                                                        |
| ----------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| init        | token:string, config:object                                  | void   | Initializes person information using **token** and **config**.                   |
| track       | event:string, properties:object, callback:function           | void   | Sends an event to Mixpanel.                                          |
| trackBatch  | eventList:array, options:object, callback:function           | void   | Sends a batch of events to Mixpanel.                                      |
| import      | event:string, time:date\|number, properties:object, callback:function | void   | Sends events to Mixpanel through an endpoint.<br>The time parameter should be a date or number, indicating the time when the event occurred. |
| importBatch | eventList:array, options:object, callback:function           | void   | Sends a list of events to Mixpanel through an endpoint.                          |
| alias       | distinctId:string, alias:string                              | void   | Creates an alias for a distinct ID.                                        |
| setConfig   | config:object                                                | void   | Modifies the configuration of Mixpanel.                                            |

### groups

| Name     | Parameter                                                        | Return Value | Description                                                |
| ----------- | ------------------------------------------------------------ | ------ | ---------------------------------------------------- |
| setOnce     | groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function | void   | Adds an property value to a group only if it has not set before. |
| set         | groupKey:string, groupId:string, properties:object, to:string, modifiers:object, callback:function | void   | Sets the attributes of a group profile.                            |
| deleteGroup | groupKey:string, groupId:string, modifiers:object, callback:function | void   | Permanently deletes a group profile.                              |
| remove      | groupKey:string, groupId:string, data:object, modifiers:object, callback:function | void   | Deletes property values from a list value group profile.                |
| union       | groupKey:string, groupId:string, data:object, modifiers:object, callback:function | void   | Merges property values into a list value group profile.                  |
| unset       | groupKey:string, groupId:string, properties:object, modifiers:object, callback:function | void   | Deletes attributes from a group profile.                            |



### people



| Name      | Parameter                                                        | Return Value | Description                                              |
| ------------ | ------------------------------------------------------------ | ------ | -------------------------------------------------- |
| setOnce      | distinctId:string, properties:object, to:string, modifiers:object, callback:function | void   | Sets a user property only if the user property has not bee set before. |
| set          | distinctId:string, properties:object, to:string, modifiers:object, callback:function | void   | Sets user properties.                    |
| increment    | distinctId:string, properties:object, by:string, modifiers:object, callback:function | void   | Increments or decrements the value of a numeric property.                 |
| append       | distinctId:string, properties:object, value:string, modifiers:object, callback:function | void   | Adds a value to a list-valued property.          |
| trackCharge  | distinctId:string, amount:number, properties:object, modifiers:object, callback:function | void   | Records the charge from the current user.                |
| clearCharges | distinctId:string, modifiers:object, callback:function       | void   | Clears all transaction data of the current user.                      |
| deleteUser   | distinctId:string, modifiers:object, callback:function       | void   | Deletes a user.                          |
| remove       | distinctId:string, data:object, modifiers:object, callback:function | void   | Removes a specific value from a list property.                |
| union        | distinctId:string, data:object, modifiers:object, callback:function | void   | Adds specified values to a list property.                  |
| unset        | distinctId:string, properties:object, modifiers:object, callback:function | void   | Unsets a specific property in the group.                  |



## Constraints
Mixpanel has been verified in the following versions:
- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)

## Directory Structure

```
|---- mixpanel  
|     |---- entry  # Sample code
|     |---- library  # Mixpanel library
|     |---- README.md  # Readme 
|     |---- README_zh.md  # Readme 
```

## How to Contribute

If you find any problem when using Mixpanel, submit an [Issue](https://gitee.com/openharmony-tpc/mixpanel-ohos/issues) or [PR](https://gitee.com/openharmony-tpc/mixpanel-ohos/pulls).

## License

This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-tpc/mixpanel-ohos/blob/master/LICENSE).
