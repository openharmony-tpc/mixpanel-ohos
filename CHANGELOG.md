## 2.0.1
- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)上验证通过

## 2.0.0
1.DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
2.Ark Ts 语法适配

## 1.0.2

1.适配DevEco Studio 3.1 Beta1及以上版本

## 1.0.1

1.关键字修改，标签添加

## 1.0.0

1. 移植适配mixpanel-android软件的6.1.0版本到OpenHarmony系统




